#!/usr/bin/env bash

set -euo pipefail
set -x

script_dir=$(dirname $(readlink -f $0))

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

setup_environment()
{
    # retrieve Depot tools
    # https://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools_tutorial.html#_setting_up
    mkdir -p depot_tools
    pushd depot_tools
    wget -q https://storage.googleapis.com/chrome-infra/depot_tools.zip
    unzip -q -o depot_tools.zip
    popd

    # cipd_bin holds ninja.exe and other windows binaries
    export PATH="$(pwd)/depot_tools/.cipd_bin:$(pwd)/depot_tools:$PATH"

    # force to get local toolchain (not in Google)
    export DEPOT_TOOLS_WIN_TOOLCHAIN=0
}

checkout()
{
    cmd.exe /c "fetch dart"
    # will fetch https://github.com/dart-lang/sdk + needed dependencies
    cd sdk
    git reset --hard $version
    git log -n1
}

build()
{
    # clean env (conflicts with their setup, at vcvarsall.bat gets called again)
    # need to keep previous path (with depot_tools added)
    cmd.exe /c "set __VSCMD_PREINIT_PATH=%PATH% && vcvarsall.bat /clean_env && python3 ./tools/build.py --arch arm64 --mode release create_sdk"
    # target create_sdk is needed, as some deps may be missing:
    # https://github.com/dart-lang/sdk/issues/50192#issuecomment-1277691460

    file out/ReleaseXARM64/*.exe out/ReleaseXARM64/*.dll
    out/ReleaseXARM64/dart.exe --version
}

run_example()
{
    cat > hello_world.dart << EOF
void main() {
  print('Hello, World!');
}
EOF

    # run directly
    out/ReleaseXARM64/dart.exe run hello_world.dart
    # compile as bytecode
    out/ReleaseXARM64/dart.exe compile kernel hello_world.dart -o hello_world.dill
    # run from bytecode
    out/ReleaseXARM64/dart.exe run hello_world.dill
}

run_tests()
{
    # run tests, print only commands to reproduce (else, it's pretty verbose)
    # ignore failing tests for now
    (python3 ./tools/test.py -mrelease --arch=arm64 --compiler=none --runtime=vm vm |&
     grep ^python3) || true
}

setup_environment
checkout
build
run_example
run_tests
